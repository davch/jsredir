## JSRedir

### Introduction:
A URL Redirector, with its database of source & destination stored as key/value pairs.

### Need:
A CNAME record `dav` of domain `inder.co.in` with value of `davchana.com` makes tumblr show a page that There is nothing here. The desire is just to cleanly forward `dav.inder.co.in` to `davchana.com`, without any masking.

### Usage:
All the key/value pairs are defined in `domains` object in javascript. A for loop checks current domain againt all the object keys, & redirects if finds a match. If no match found, `redirect to davchana.com`

###  Setup:

1. Add the full name with subdomain & its destination in key/value pair.
2. Add a CNAME is domain's control panel in Domain Registrar's website, with subdomain as host & value as `davch.gitlab.io` as value.
3. Add domain in this project's Pages.